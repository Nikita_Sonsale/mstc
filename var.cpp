#include <windows.h>
#include <stdio.h>


int CDECL MessageBoxPrint(TCHAR* szCaption, TCHAR* szFormat,...)
{
    TCHAR buffer[1024];
    va_list argList;
    va_start(argList,szFormat);
    vsprintf(buffer,szFormat,argList);
    va_end(argList);
    return MessageBox((HWND)NULL,buffer,szCaption,MB_OK);
}
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    int cxScreen,cyScreen;
    cxScreen = GetSystemMetrics(SM_CXSCREEN);
    cyScreen = GetSystemMetrics(SM_CYSCREEN);

    MessageBoxPrint(TEXT("screen size"),TEXT("My screen size is %d X %d"),cxScreen,cyScreen);
    return EXIT_SUCCESS;
}