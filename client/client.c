#include<stdio.h>
#include<stdlib.h>
#include "arithmetic.h"
int main()
{
	int num1;
	int num2;
	int choice = 0;
	int result;
	printf("Enter first number : ");
	scanf("%d", &num1);

	printf("Enter second number : ");
	scanf("%d", &num2);

    printf("*****************\n");
	printf("1.ADDITION\n");
	printf("2.SUBTRACTION\n");
	printf("3.MULTIPLICATION\n");
    printf("4.DIVISON\n");
    printf("*****************\n");

    do
	{
		printf("Enter choice : ");
		scanf("%d",&choice);
		switch (choice)
	    {
			case 1:
				result = Add(num1,num2); 
				printf("ADDITION : %d\n\n",result);
				break;

			case 2:
				result = Sub(num1,num2);
            	printf("SUBTRACTION : %d\n\n",result);
				break;

			case 3:
				result = Mul(num1,num2);
            	printf("MULTIPLICATION : %d\n\n",result);
				break;
        	
        	case 4:
				result = Div(num1,num2);
            	printf("DIVISON : %d\n\n",result);
				break;

			default:
				printf("Enter valid Choice\n");
		}
	} while (choice != 5);
	exit(0);
}

