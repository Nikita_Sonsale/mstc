#ifndef ARITHMETIC_H
#define ARITHMETIC_H

int Add(int n1, int n2);
int Sub(int n1, int n2);
int Mul(int n1, int n2);
int Div(int n1, int n2);

#endif
